﻿using PricelistService.Core.Generators;
using System.Web;
using System.Web.Http;

namespace PricelistService
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            var simplePricelistGenerator = new SimplePricelistGenerator();
            var simplePricelist = simplePricelistGenerator.Generate();
            HttpContext.Current.Application["InMemoryPricelist"] = simplePricelist;
        }
    }
}

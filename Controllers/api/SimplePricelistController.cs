using PricelistService.Core;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PricelistService.Controllers.api
{
    public class SimplePricelistController : ApiController
    {
        private readonly ISimplePricelistRepository _simplePricelistRepository;

        public SimplePricelistController(ISimplePricelistRepository simplePricelistRepository)
        {
            _simplePricelistRepository = simplePricelistRepository;
        }

        public HttpResponseMessage Get()
        {
            HttpResponseMessage response;
            try
            {
                var simplePricelist = _simplePricelistRepository.Get();
                response = Request.CreateResponse(HttpStatusCode.OK, simplePricelist);
            }
            catch (HttpResponseException e)
            {
                return Request.CreateErrorResponse(e.Response.StatusCode, e.Response.ReasonPhrase);
            }
            catch (System.Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error");
            }
            return response;
        }
    }
}
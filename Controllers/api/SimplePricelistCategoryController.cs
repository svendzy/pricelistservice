using PricelistService.Core;
using PricelistService.Core.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PricelistService.Controllers.api
{
    public class SimplePricelistCategoryController : ApiController
    {
        private readonly ISimplePricelistRepository _simplePricelistRepository;

        public SimplePricelistCategoryController(ISimplePricelistRepository simplePricelistRepository)
        {
            _simplePricelistRepository = simplePricelistRepository;
        }

        public HttpResponseMessage Get(int categoryId)
        {
            HttpResponseMessage response;
            try
            {
                var simplePricelistCategory = _simplePricelistRepository.GetCategory(categoryId);
                response = Request.CreateResponse(HttpStatusCode.OK, simplePricelistCategory);
            }
            catch (HttpResponseException e)
            {
                return Request.CreateErrorResponse(e.Response.StatusCode, e.Response.ReasonPhrase);
            }
            catch (System.Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error");
            }
            return response;
        }

        public HttpResponseMessage Put(int categoryId, [FromBody]PricelistCategory category)
        {
            try
            {
                _simplePricelistRepository.UpdateCategory(categoryId, category);
            }
            catch (HttpResponseException e)
            {
                return Request.CreateErrorResponse(e.Response.StatusCode, e.Response.ReasonPhrase);
            }
            catch (System.Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error");
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        public HttpResponseMessage Delete(int categoryId)
        {
            try
            {
                _simplePricelistRepository.DeleteCategory(categoryId);
            }
            catch (HttpResponseException e)
            {
                return Request.CreateErrorResponse(e.Response.StatusCode, e.Response.ReasonPhrase);
            }
            catch (System.Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error");
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
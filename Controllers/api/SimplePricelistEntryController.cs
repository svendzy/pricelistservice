using PricelistService.Core;
using PricelistService.Core.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PricelistService.Controllers.api
{
    public class SimplePricelistEntryController : ApiController
    {
        private readonly ISimplePricelistRepository _simplePricelistRepository;

        public SimplePricelistEntryController(ISimplePricelistRepository simplePricelistRepository)
        {
            _simplePricelistRepository = simplePricelistRepository;
        }

        public HttpResponseMessage Get(int categoryId, int entryId)
        {
            HttpResponseMessage response;
            try
            {
                var simplePricelistEntry = _simplePricelistRepository.GetEntry(categoryId, entryId);
                response = Request.CreateResponse(HttpStatusCode.OK, simplePricelistEntry);
            }
            catch (HttpResponseException e)
            {
                return Request.CreateErrorResponse(e.Response.StatusCode, e.Response.ReasonPhrase);
            }
            catch (System.Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error");
            }
            return response;
        }

        public HttpResponseMessage Put(int categoryId, int entryId, [FromBody]PricelistEntry entry)
        {
            try
            {
                _simplePricelistRepository.UpdateEntry(categoryId, entryId, entry);
            }
            catch (HttpResponseException e)
            {
                return Request.CreateErrorResponse(e.Response.StatusCode, e.Response.ReasonPhrase);
            }
            catch (System.Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error");
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        public HttpResponseMessage Delete(int categoryId, int entryId)
        {
            try
            {
                _simplePricelistRepository.DeleteEntry(categoryId, entryId);
            }
            catch (HttpResponseException e)
            {
                return Request.CreateErrorResponse(e.Response.StatusCode, e.Response.ReasonPhrase);
            }
            catch (System.Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error");
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PricelistService.Controllers.api
{
    public class DefaultController : ApiController
    {
        public HttpResponseMessage Get(string uri = "")
        {
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Not Found");
        }
    }
}

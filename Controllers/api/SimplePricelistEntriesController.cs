using PricelistService.Core;
using PricelistService.Core.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PricelistService.Controllers.api
{
    public class SimplePricelistEntriesController : ApiController
    {
        private readonly ISimplePricelistRepository _simplePricelistRepository;

        public SimplePricelistEntriesController(ISimplePricelistRepository simplePricelistRepository)
        {
            _simplePricelistRepository = simplePricelistRepository;
        }

        public HttpResponseMessage Get(int categoryId)
        {
            HttpResponseMessage response;
            try
            {
                var simpePricelistEntries = _simplePricelistRepository.GetEntries(categoryId);
                response = Request.CreateResponse(HttpStatusCode.OK, simpePricelistEntries);
            }
            catch (HttpResponseException e)
            {
                return Request.CreateErrorResponse(e.Response.StatusCode, e.Response.ReasonPhrase);
            }
            catch (System.Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error");
            }
            return response;
        }

        public HttpResponseMessage Post(int categoryId, [FromBody]PricelistEntry entry)
        {
            try
            {
                _simplePricelistRepository.AddEntry(categoryId, entry);
            }
            catch (HttpResponseException e)
            {
                return Request.CreateErrorResponse(e.Response.StatusCode, e.Response.ReasonPhrase);
            }
            catch (System.Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error");
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
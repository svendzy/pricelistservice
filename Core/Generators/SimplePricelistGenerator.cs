﻿using PricelistService.Core.Models;
using System;
using System.Collections.Generic;

namespace PricelistService.Core.Generators
{
    public class SimplePricelistGenerator
    {
        public SimplePricelist Generate()
        {
            var rnd = new Random(2000);

            var pricelistCategories = new List<PricelistCategory>();

            for(var nCategory=0; nCategory < 4; nCategory++)
            {
                var pricelistEntries = new List<PricelistEntry>();
                for (var nEntry=0; nEntry < 50; nEntry++)
                {
                    pricelistEntries.Add(new PricelistEntry()
                    {
                        Id = nEntry,
                        Name = string.Concat("Product", nEntry),
                        Price = (float)rnd.Next(1, 450),
                        Order = nEntry
                    });
                }
                pricelistCategories.Add(
                    new PricelistCategory()
                    {
                        Id = nCategory,
                        Name ="Category " + nCategory,
                        Order = nCategory,
                        Entries = pricelistEntries
                    });
            }

            var simplePricelist = new SimplePricelist
            {
                Name = "SimplePricelist",
                Description = "SimplePricelist Description",
                LastUpdated = DateTime.Now,
                Categories = pricelistCategories
            };
           return simplePricelist;
        }
    }
}
﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PricelistService.Core.Models
{
    public class SimplePricelist : Pricelist
    {
        [JsonProperty(Order = 4)]
        public IList<PricelistCategory> Categories { get; set; }
    }
}